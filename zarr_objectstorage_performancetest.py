#!/usr/bin/env python
# coding: utf-8

# Dieses Skript ist eine Kopie des Python-Notebooks "zarr_objectstorage_performancetest.ipynb". Es wurde hier in eine Python-Datei umgewandelt, um die Integration von cProfile zu ermöglichen. Leider konnte cProfile keine Einsichten liefern. Die Ergebnisse sind in der Datei "zarr_performance_cProfile.txt".

def zarr_performancetest():

    PYTHONPATH = '/sw/rhel6-x64/conda/anaconda3-bleeding_edge'
    
    import dask.array as dsa
    import xarray
    import intake
    import zarr
    import fsspec
    import numpy as np
    import pandas as pd
    from contextlib import contextmanager
    import time
    import dask
    import json
    from datetime import datetime

    from dask.distributed import Client

    hardware = '256GB 24Cores'

    #use intake to browse catalog
    col_url = "https://swift.dkrz.de/v1/dkrz_a44962e3ba914c309a7421573a6949a6/intake-esm/swift-cmip6.json"
    col = intake.open_esm_datastore(col_url)

    cat = col.search(variable_id="tasmax", table_id="day")
    zarrpath = cat.unique("zarr_path")["zarr_path"]["values"][0]
    fsmap = fsspec.get_mapper(zarrpath)
    openzarr = xarray.open_zarr(fsmap, consolidated=True)
    data = openzarr.tasmax.data
    data

    class DevNullStore:

        def __init__(self):
            pass

        def __setitem__(*args, **kwargs):
            pass

    null_store = DevNullStore()
    # this line produces no error but actually does nothing
    null_store['foo'] = 'bar'

    
    # ## Set up parallel environment


    class DiagnosticTimer:
        def __init__(self):
            self.diagnostics = []

        @contextmanager
        def time(self, **kwargs):
            tic = time.time()
            yield
            toc = time.time()
            kwargs["runtime"] = toc - tic
            kwargs["throughput_MBps"] = data.nbytes/ 1e6/ (toc-tic)
            self.diagnostics.append(kwargs)

        def dataframe(self):
            return pd.DataFrame(self.diagnostics)

    diag_timer = DiagnosticTimer()

    chunksize = np.prod(data.chunksize) * data.dtype.itemsize

    diag_kwargs = dict(nbytes=data.nbytes, chunksize=chunksize,
                       cloud='swift', format='zarr', hardware=hardware, datetime=datetime.now())

    configuration = pd.read_csv('clusterConfiguration.csv')

    for i in configuration.index:

        memory_limit="48GB"
        threads = configuration.threads[i].item()
        nworker = configuration.nworker[i].item()

        client = Client(processes=True, threads_per_worker=threads, n_workers=nworker, memory_limit=memory_limit)

        def total_nthreads():
            return sum([v for v in client.nthreads().values()])
        def total_ncores():
            return sum([v for v in client.ncores().values()])
        def total_workers():
            return len(client.ncores())


        with diag_timer.time(nthreads=total_nthreads(),
                                 ncores=total_ncores(),
                                 nworkers=total_workers(),
                                 **diag_kwargs):

                future = dsa.store(data, null_store, lock=False, compute=False)
                dask.compute(future, retries=5)

        client.close()

    diag_timer.dataframe().to_csv('/pf/b/b381359/pre_project_performancetests/resultsPerformancetest.csv', mode='a', header=False, index=False)


def main():
    zarr_performancetest()

if __name__ == '__main__':
    import cProfile
    import pstats
    import io
    
    #cProfile.run('main()', "output.dat")
    pr = cProfile.Profile()
    pr.enable()
    
    result = main()
    
    pr.disable()
    s = io.StringIO()
    ps = pstats.Stats(pr, stream=s).sort_stats('tottime')
    ps.print_stats()

    with open('zarr_performance_cProfile.txt', 'w+') as f:
        f.write(s.getvalue())


