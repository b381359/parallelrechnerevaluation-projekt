This project measures and compares the performance of the file formats NetCDF and Zarr with different levels of parallelisation.

The project is based on Ryan Abernathey's paper: Big Arrays, Fast: Profiling Cloud Storage Read Throughput:

http://gallery.pangeo.io/repos/earthcube2020/ec20_abernathey_etal/cloud_storage.html

All tests were conducted on the Servers of the German Climate Computing Centre (DKRZ). 

Zarr was tested on the DKRZ object storage. NetCDF was tested on the DKRZ Mistral HPC.

For NetCDF, two types of access were tested:

1. NetCDF access using the OPeNDAP protocol
2. NetCDF access using a simple file path

The results show that Zarr on the DKRZ object storage has a higher read-throughput than the nowadays-used OPeNDAP protocol.

However, NetCDF access using the file path is performing better than Zarr. I'm currently working on figuring out why that might be the case.
